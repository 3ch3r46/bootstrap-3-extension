<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="row">
    <div class="col-md-9">
        <div id="content">
            <?php echo $content; ?>
        </div><!-- content -->
    </div>
    <div class="col-md-3 sidebar-nav">
        <?php
        if($this->affixMenu==true) $this->beginWidget('Bt3Affix',array('htmlOptions'=>array('offsetTop'=>'.navbar','offsetBottom'=>"#footer",'parentClass'=>'sidebar')));
            BS3::menu(array(
                'items'=>$this->menu,
            	'type' => 'list',
            	'htmlOptions'=>array('class'=>'well'),
            ));
        if($this->affixMenu==true) $this->endWidget();
        ?>
        <!-- sidebar -->
    </div>
</div>
<?php $this->endContent(); ?>