<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <!-- link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/styles.css" /-->
	
	<!-- link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/font-awesome/css/font-awesome.min.css"-->
	<!--[if IE 7]>
	  <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/font-awesome/css/font-awesome-ie7.min.css">
	<![endif]-->
	
	<?php Yii::app()->bootstrap->registerAllCss(); ?>
	<?php Yii::app()->bootstrap->registerCoreScripts(); ?>
</head>

<?php 
//<body class="background-primary">
$htmlBodyOptions=array();
if(isset($this->background))
	$htmlBodyOptions['class']='background-'.$this->background;
echo CHtml::openTag('body',$htmlBodyOptions);
?>
<!-- Wrap all page content here -->
<div id="wrap">

<?php if($this->useNavbar==true): ?>
<?php $this->widget('bootstrap.widgets.TbNavbar',array(
	'collapse'=>true,
	'type'=>'primary',
    'items'=>require(Yii::app()->basePath.'/config/navbar.php'),
)); ?>
<?php endif;?>

<div id="page" class="<?php if(isset($this->jumbotron['heading'])) echo "page-with-jumbotron";?>"></div>

<?php if(isset($this->jumbotron['heading'])):
$this->beginWidget('bootstrap.widgets.TbJumboTron',$this->jumbotron);?>
<?php echo $this->jumboContent; ?>
<?php $this->endWidget();endif;?>

<div class="container">
		
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs',array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

</div><!-- container -->
</div><!-- wrap -->
<?php if($this->useFooter==true):?>
<div id="footer">
	<div class="container">
		<p class="text-muted credit"><?php if(isset(Yii::app()->params->footer)) echo Yii::app()->params->footer;?></p>
	</div>
</div><!-- footer -->
<?php endif;?>

</body>

</html>
