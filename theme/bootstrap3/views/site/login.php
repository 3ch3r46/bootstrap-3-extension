<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form TbActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/signin.css');
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions'=>array(
		'class'=>'form-signin',
	),
)); ?>
	<h2 class="form-signin-heading">Please sign in</h2>
	
	<?php echo $form->textFieldRow($model, 'username',array('labelToPlaceHolder'=>true,'useLabel'=>false,'useFormGroup'=>false));?>
	
	<?php echo $form->passwordFieldRow($model, 'password',array('labelToPlaceHolder'=>true,'useLabel'=>false,'useFormGroup'=>false))?>

	<?php echo $form->checkBoxRow($model, 'rememberMe'); ?>

	<?php $this->widget('bootstrap.widgets.TbButton',array(
			'label'=>'Sign In',
			'buttonType'=>'submit',
			'type'=>'primary',
			'size'=>'lg',
			'block'=>true,
		));?>

<?php $this->endWidget(); ?>