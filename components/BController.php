<?php
class BController extends Controller
{
	public $_jumbotron=array('type'=>'white','textType'=>'primary');
	
	public $jumboContent;
	
	public $useNavbar=true;
	
	public $useFooter=true;
	
	public $background;
	
	public $affixMenu=false;
	
	public function setJumbotron($jumbotron)
	{
		$this->_jumbotron=CMap::mergeArray($this->_jumbotron, $jumbotron);
	}
	
	public function getJumbotron()
	{
		return $this->_jumbotron;
	}
	
}