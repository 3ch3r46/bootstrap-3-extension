<?php
/**
 * TbDatePicker class file.
 * 
 * @author Moh Khoirul Anam <anam@solusiq.com>
 * @copyright Moh Khoirul Anam &copy; 2013
 * @package bootstrap.widgets
 */
Yii::setPathOfAlias('boot', dirname(__FILE__).'/../');
Yii::import('boot.components.Bootstrap');
class TbDatePicker extends CWidget
{
	/**
	 * @var string format of date picker
	 */
	public $dateFormat='yyyy-mm-dd';
	
	/**
	 * @var array the HTML attributes for this widget.
	 */
	public $htmlOptions=array();
		
	/**
	 * @var string name of date picker input.
	 */
	public $name="datePicker";
	
	/**
	 * @var array options of javascript date picker.
	 */
	public $options=array();
	
	public $dateOptions=array();
	
	/**
	 * @var string on options of on change action.
	 */
	public $onChangeOptions=" ";
	
	/**
	 * @var boolean close on change the datepicker.
	 */
	public $closeOnChange=true;
	/**
	 * @var string value for date picker input.
	 */
	public $value;
	
	public $id;
	
	public $icon='calendar';
	
	public $enableIcon=true;
	
	/**
	 * initialize widgets(non-PHPdoc)
	 * @see CWidget::init()
	 */
	public function init(){
		$classes=array('form-control');
				
		if (isset($this->dateFormat)) {
			$this->dateOptions['data-date-format']=$this->dateFormat;
		}
		
		$this->dateOptions['class']='input-group date';
		
		if (isset($this->htmlOptions['id'])){
			$this->id=$this->htmlOptions['id']."_datepicker";
			$this->dateOptions['id']=$this->id;
		}else{
			$this->id="date_".rand(00, 99);
			$this->dateOptions['id']=$this->id;
		}
		
		if(isset($this->htmlOptions['name'])){
			$this->name=$this->htmlOptions['name'];
			unset($this->htmlOptions['name']);
		}
		
		if (!empty($classes))
		{
			$classes = implode(' ', $classes);
			if (isset($this->htmlOptions['class']))
				$this->htmlOptions['class'] .= ' '.$classes;
			else
				$this->htmlOptions['class'] = $classes;
		}
		
		if(!isset($this->htmlOptions['readonly']))
			$this->htmlOptions['readonly']=true;
		
		if($this->htmlOptions['readonly']==true){
			$this->htmlOptions['readonly']="readonly";
		}else{
			unset($this->htmlOptions['readonly']);
		}
		
		if($this->onChangeOptions==true){
			$this->onChangeOptions.="jQuery('.datepicker').hide()";
		}
	}
	
	/**
	 * run this widgets.(non-PHPdoc)
	 * @see CWidget::run()
	 */
	public function run(){
		echo CHtml::openTag('div',$this->dateOptions);
		echo CHtml::textField($this->name,$this->value,$this->htmlOptions);
		if($this->enableIcon) echo CHtml::tag('span',array('class'=>'input-group-addon'),CHtml::tag('i',array('class'=>Bootstrap::app()->iconPrefix.' '.Bootstrap::app()->iconPrefix.'-calendar'),'',true),true);
		echo CHtml::closeTag('div');
		
		Bootstrap::helper()->registerDatePicker();
		
		$options = !empty($this->options) ? CJavaScript::encode($this->options) : '';
		$onChangeOptions = $this->onChangeOptions;

		Yii::app()->clientScript->registerScript('datepicker_'.$this->id,"jQuery('#{$this->id}').datepicker({$options}).on('changeDate',function(){{$onChangeOptions} })");
	}
}