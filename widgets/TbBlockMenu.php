<?php

class TbBlockMenu extends TbMenu
{
	
	public $type;
	
	public function init(){
		parent::init();
		if(isset($this->htmlOptions['class']))
			$this->htmlOptions['class'].=' block-menu';
		else 
			$this->htmlOptions['class']='block-menu';
		
		if(isset($this->type))
			$this->htmlOptions['class'].=' block-'.$this->type;
 		if(!isset($this->htmlOptions['id']))
 			$this->htmlOptions['id']="BlockMenu".rand(00,99);
		$id=$this->htmlOptions['id'];
		if($this->justified==true){
			$width=100/count($this->items);
			Yii::app()->clientScript->registerCss("block-menu","
			#{$id}.block-menu > li{
				width:{$width}%;
			}
			");
		}
	}
	
	protected function renderMenuItem($item)
	{
		if (isset($item['icon']))
		{
			if (strpos($item['icon'], Bootstrap::app()->iconPrefix) === false)
			{
				$pieces = explode(' ', $item['icon']);
				$item['icon'] = Bootstrap::app()->iconPrefix.'-'.implode(' '.Bootstrap::app()->iconPrefix.'-', $pieces);
			}
		
			$item['label'] = '<i class="'.Bootstrap::app()->iconPrefix.' '.$item['icon'].'"></i> '.$item['label'];
		}
		
		if (isset($item['items']) && !empty($item['items']))
		{
			$item['url'] = '#';
		
			if (isset($item['linkOptions']['class']))
				$item['linkOptions']['class'] .= ' dropdown-toggle';
			else
				$item['linkOptions']['class'] = 'dropdown-toggle';
		
			$item['linkOptions']['data-toggle'] = 'dropdown';
			$item['label'] .= '<span class="caret"></span>';
		}
		
		$item['label'].=CHtml::tag('span',array('class'=>'three-d-box'),CHtml::tag('span',array('class'=>'front'),$item['label'],true).CHtml::tag('span',array('class'=>'back'),$item['label'],true),true);
		if(isset($item['linkOptions']['class']))
			$item['linkOptions']['class'].=' three-d';
		else
			$item['linkOptions']['class']='three-d';
		
		if (!isset($item['linkOptions']))
			$item['linkOptions'] = array();
		
		if (isset($item['url']))
			return CHtml::link($item['label'], $item['url'], $item['linkOptions']);
		else
			return $item['label'];
	}
}