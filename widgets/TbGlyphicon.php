<?php
/**
 * TbGlyphicon class file
 * @author Moh Khoirul Anam <anam@solusiq.com>
 * @copyright Copyright &copy; Moh Khoirul Anam 2013-
 * @package bootstrap.widgets
 */
Yii::setPathOfAlias('components', dirname(__FILE__).'/../components');
Yii::import('components.*');

class TbGlyphicon extends CWidget
{
	/**
	 * @var string of icon
	 */
	public static $icon;
	
	public static $htmlOptions;
	
	/**
	 * run this widgets(non-PHPdoc)
	 * @see CWidget::run()
	 */
	public function run(){
		$htmlOptions=self::$htmlOptions;
		$iconPrefix = Bootstrap::getSIconPrefix();
		if(isset($htmlOptions['class'])) 
			$htmlOptions['class'] .= ' '. $iconPrefix . ' ' . $iconPrefix . '-' . self::$icon;
		else 
			$htmlOptions['class'] = $iconPrefix . ' ' . $iconPrefix . '-' . self::$icon;
		return CHtml::tag('i',$htmlOptions,'',true);
	}
	
	public static function getIcon($icon,$htmlOptions=array()){
		self::$icon=$icon;
		self::$htmlOptions=$htmlOptions;
		return @self::run();
	}
}