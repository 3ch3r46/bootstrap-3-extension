<?php
/**
 * TbListGroup class file.
 * @author Moh Khoirul Anam <anam@solusiq.com>
 * @copyright Copyright &copy; Moh Khoirul Anam 2013-
 * @package bootstrap.widgets
 */
Yii::setPathOfAlias('components', dirname(__FILE__).'/../components');
Yii::import('components.*');
Yii::setPathOfAlias('widgets',dirname(__FILE__).'/../widgets');
Yii::import('widgets.*');

class TbListGroup extends CWidget
{
	//list group type
	const TYPE_PRIMARY = 'primary';
	const TYPE_INFO = 'info';
	const TYPE_SUCCESS = 'success';
	const TYPE_WARNING = 'warning';
	const TYPE_DANGER = 'danger';
	/**
	 * @var array of list items.
	 */
	public $items=array();
	/**
	 * @var array the HTML attributes of list group
	 */
	public $htmlOptions=array();
	/**
	 * @var string type of list group.
	 */
	public $type;
	/**
	 * @var string icon
	 */
	public $icon;
	
	/**
	 * initialize widgets(non-PHPdoc)
	 * @see CWidget::init()
	 */
	public function init(){
		$classes = array('list-group');
		
		$validTypes = array(self::TYPE_PRIMARY, self::TYPE_INFO, self::TYPE_SUCCESS,self::TYPE_WARNING, self::TYPE_DANGER);
		
		if (isset($this->type) && in_array($this->type, $validTypes))
			$classes[] = 'list-group-'.$this->type;
		
		if (!empty($classes))
		{
			$classes = implode(' ', $classes);
			if (isset($this->htmlOptions['class']))
				$this->htmlOptions['class'] .= ' '.$classes;
			else
				$this->htmlOptions['class'] = $classes;
		}
	}
	/**
	 * Create the list
	 */
	public function createList(){
		foreach ($this->items as $data){
			$text=null;
			$icon=null;
			if (isset($this->icon))
				$icon = $this->icon;
			if (isset($data['icon']))
				$icon = $data['icon'];
			if (isset($data['icon']) and $data['icon']==false)
				unset($icon);
						
			if(isset($data['text'])) {
				if(is_array($data['text']))
				{	
						if(isset($data['text']['heading'])){
							if(isset($icon)){
								$data['text']['heading']=TbGlyphicon::getIcon($icon).' '.$data['text']['heading'];
								unset($icon);
							}
							if(isset($data['badge'])){
								$data['text']['heading'].=CHtml::tag('span',array('class'=>'badge pull-right'),$data['badge']);
								unset($data['badge']);
							}
							$text.=CHtml::tag('h4',array('class'=>'list-group-item-heading'),$data['text']['heading']);//'<h4 class="list-group-item-heading">'.$data['text']['heading'].'</h4>';
						}
						if(isset($data['text']['text']))
							$text.=CHtml::tag('p',array('class'=>'list-group-item-text'),$data['text']['text']);//'<p class="list-group-item-text">'.$data['text']['text'].'</p>';
				} else {
					$text=$data['text'];
					if(isset($icon))
						$text=TbGlyphicon::getIcon($icon) . ' ' . $text;
						
					if(isset($data['badge']))
						$text.=CHtml::tag('span',array('class'=>'badge'),$data['badge']);
				}
			} elseif (isset($data['heading'])) {
				$text=$data['heading'];
				if (isset($icon))
					$text=TbGlyphicon::getIcon($icon).' '.$text;
			}
			
			$htmlOptions=isset($data['htmlOptions']) ? $data['htmlOptions'] : array();
			!isset($htmlOptions['class']) ? $htmlOptions['class']='list-group-item' : $htmlOptions['class'].=' list-group-item';
			
			if(isset($data['active']) and $data['active']==true)
				$htmlOptions['class'].=" active";
			if(isset($data['url'])){
				if(is_array($data['url']))
					$url=Yii::app()->createUrl($data['url']);
				else $url=$data['url'];
			}else{
				$url=null;
			}
			if (isset($data['text']))
				echo CHtml::link($text,$url,$htmlOptions);
			elseif (isset($data['heading']))
				echo CHtml::tag('span', $htmlOptions, $text, true);
		}
	}
	/**
	 * runt this widgets(non-PHPdoc)
	 * @see CWidget::run()
	 */
	public function run(){
		echo CHtml::openTag('div',$this->htmlOptions);
		
		$this->createList();
		
		echo CHtml::closeTag('div');
	}
}